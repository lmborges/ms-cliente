# Ms Cliente

# Versão: 1.0.0

MVP para atender ao Platform Builders Challenge
## 1. Setup
É necessário ter uma instalação do [Docker-Compose](https://docs.docker.com/compose/) ou [Podman-Compose](https://fedoramagazine.org/manage-containers-with-podman-compose/) para continuar.

#### 1.1 Instalação do Docker/ Podman / Docker Compose / Podman Compose
1. Pré-Requisitos para implantação
    * Hardware Mínimos
        * 8Gb RAM
        * 20Gb HD lives
        * Processador de 2 núcleos com 2GHz
    * Software
        * Linux Ubuntu Server ou equivalente. 
    * Permissão de `root` para comandos de instalação.

1. Referências:
    * [Instalação Docker](https://docs.docker.com/engine/install/ubuntu/)
    * [Instalação Docker Compose](https://docs.docker.com/compose/install/) 
    * [Referência Dockerfile](https://docs.docker.com/engine/reference/builder/)
    * [Referência docker-compose.yml](https://docs.docker.com/compose/compose-file/)



#### 1.2 Implantação
A implantação local se inicia com a disponibilização dos containers de infraestrutura e então execução da aplicação.

1. Instalando containers de infraestrutura. 
    * Realizar o `pull`das imagens do `Elasticsearch` e `PostgreSQL`. 
        ```bash
        $ podman-compose -f ./docker-compose.yml pull 
        ```
    * Ao final do comando a saída deve ser semelhante a informada abaixo:
        ```bash
        using podman version: podman version 3.1.0
        podman pull postgres
        Resolved "postgres" as an alias (/home/lborges/.cache/containers/short-name-aliases.conf)
        Trying to pull docker.io/library/postgres:latest...
        Getting image source signatures
        Copying blob 03790957a916 done  
        Copying blob 54f6491bd120 done  
        Copying blob f7ec5a41d630 done  
        Copying blob b3776ac15dab done  
        Copying blob d073cd070242 done  
        Copying blob 7144fd00aec4 done  
        Copying blob 247ab23c6036 done  
        Copying blob 57800498c536 done  
        Copying blob bcb15a4d14f4 done  
        Copying blob cfc751ecbc6e done  
        Copying blob 453056a20de6 done  
        Copying blob bbf042afd4a4 done  
        Copying blob d5b1a75378ef done  
        Copying blob 7841e2074775 done  
        Copying config 26c8bcd8b7 done  
        Writing manifest to image destination
        Storing signatures
        26c8bcd8b71919f393464e595670e633e4f90c6981df4322d8c4a39332b0bd1e
        0
        podman pull docker.elastic.co/elasticsearch/elasticsearch:7.9.3
        Trying to pull docker.elastic.co/elasticsearch/elasticsearch:7.9.3...
        Getting image source signatures
        Copying blob c84049f0f38a done  
        Copying blob a9cfa40c5418 done  
        Copying blob f1feca467797 done  
        Copying blob 0daf89d8c887 done  
        Copying blob 39d9d8875a6d done  
        Copying blob e7a6547bd7a1 done  
        Copying blob 0d768056b087 done  
        Copying blob 9068e5a839f5 done  
        Copying blob 0d190ac61a88 done  
        Copying config 1ab13f928d done  
        Writing manifest to image destination
        Storing signatures
        1ab13f928dc8aa958574d060fde595bd7715c1c2d3260446356a6a02d231e168
        0
        ```
    * Iniciar o projeto em background.
        ```bash
        $ podman-compose -f ./docker-compose.yml up -d 
        ```

    * A saída de sucesso deste comando será semelhante:
        ```bash

        using podman version: podman version 3.1.0
        podman pod create --name=ms-cliente --share net -p 9200:9200 -p 5432:5432
        c3aba6e085e717ef4ddf0b15004053a96a17a81e8f96ced98533c82121c2f2b4
        0
        podman volume inspect ms-cliente_data1 || podman volume create ms-cliente_data1
        Error: error inspecting object: no such volume ms-cliente_data1
        podman run --name=es01 -d --pod=ms-cliente --label io.podman.compose.config-hash=123 --label io.podman.compose.project=ms-cliente --label io.podman.compose.version=0.0.1 --label com.docker.compose.container-number=1 --label com.docker.compose.service=es01 -e node.name=es01 -e cluster.name=es-docker-cluster -e discovery.seed_hosts=es02,es03 -e cluster.initial_master_nodes=es01,es02,es03 -e xpack.security.enabled=false -e TAKE_FILE_OWNERSHIP=true -e ES_JAVA_OPTS=-Xms512m -Xmx512m -v ms-cliente_data1:/usr/share/elasticsearch/data --add-host es01:127.0.0.1 --add-host es01:127.0.0.1 --add-host es02:127.0.0.1 --add-host es02:127.0.0.1 --add-host es03:127.0.0.1 --add-host es03:127.0.0.1 --add-host postgres-compose:127.0.0.1 --add-host ms-cliente_postgres-compose_1:127.0.0.1 --ulimit nofile=65536:65536 docker.elastic.co/elasticsearch/elasticsearch:7.9.3
        b66206f375d707551d6c539ba54f991ba1ee6fd2e5bcc171b3dba3d3135a0fb1
        0
        podman volume inspect ms-cliente_data2 || podman volume create ms-cliente_data2
        Error: error inspecting object: no such volume ms-cliente_data2
        podman run --name=es02 -d --pod=ms-cliente --label io.podman.compose.config-hash=123 --label io.podman.compose.project=ms-cliente --label io.podman.compose.version=0.0.1 --label com.docker.compose.container-number=1 --label com.docker.compose.service=es02 -e node.name=es02 -e cluster.name=es-docker-cluster -e discovery.seed_hosts=es01,es03 -e cluster.initial_master_nodes=es01,es02,es03 -e xpack.security.enabled=false -e TAKE_FILE_OWNERSHIP=true -e ES_JAVA_OPTS=-Xms512m -Xmx512m -v ms-cliente_data2:/usr/share/elasticsearch/data --add-host es01:127.0.0.1 --add-host es01:127.0.0.1 --add-host es02:127.0.0.1 --add-host es02:127.0.0.1 --add-host es03:127.0.0.1 --add-host es03:127.0.0.1 --add-host postgres-compose:127.0.0.1 --add-host ms-cliente_postgres-compose_1:127.0.0.1 --ulimit nofile=65536:65536 docker.elastic.co/elasticsearch/elasticsearch:7.9.3
        5da63ae95466c5c4f39cd0100b9d4bb63078ff6eca837a237149db9be2112410
        0
        podman volume inspect ms-cliente_data3 || podman volume create ms-cliente_data3
        Error: error inspecting object: no such volume ms-cliente_data3
        podman run --name=es03 -d --pod=ms-cliente --label io.podman.compose.config-hash=123 --label io.podman.compose.project=ms-cliente --label io.podman.compose.version=0.0.1 --label com.docker.compose.container-number=1 --label com.docker.compose.service=es03 -e node.name=es03 -e cluster.name=es-docker-cluster -e discovery.seed_hosts=es01,es02 -e cluster.initial_master_nodes=es01,es02,es03 -e xpack.security.enabled=false -e TAKE_FILE_OWNERSHIP=true -e ES_JAVA_OPTS=-Xms512m -Xmx512m -v ms-cliente_data3:/usr/share/elasticsearch/data --add-host es01:127.0.0.1 --add-host es01:127.0.0.1 --add-host es02:127.0.0.1 --add-host es02:127.0.0.1 --add-host es03:127.0.0.1 --add-host es03:127.0.0.1 --add-host postgres-compose:127.0.0.1 --add-host ms-cliente_postgres-compose_1:127.0.0.1 --ulimit nofile=65536:65536 docker.elastic.co/elasticsearch/elasticsearch:7.9.3
        6a180f96cf7da9425ef6bd322c54648ec254f5753a8888cc0b74b5ace3084ed2
        0
        podman volume inspect ms-cliente_data4 || podman volume create ms-cliente_data4
        Error: error inspecting object: no such volume ms-cliente_data4
        podman run --name=ms-cliente_postgres-compose_1 -d --pod=ms-cliente --label io.podman.compose.config-hash=123 --label io.podman.compose.project=ms-cliente --label io.podman.compose.version=0.0.1 --label com.docker.compose.container-number=1 --label com.docker.compose.service=postgres-compose -e POSTGRES_PASSWORD=advsolucoes.com.2021! -v ms-cliente_data4:/var/lib/postgresql/data --add-host es01:127.0.0.1 --add-host es01:127.0.0.1 --add-host es02:127.0.0.1 --add-host es02:127.0.0.1 --add-host es03:127.0.0.1 --add-host es03:127.0.0.1 --add-host postgres-compose:127.0.0.1 --add-host ms-cliente_postgres-compose_1:127.0.0.1 postgres
        0f92f517c1636c4a7ce1e493340867ec1e10274f91c103292832b8fe3b289591

        ```
1. Iniciando a aplicação
    * `Compilar` a aplicação
        ```bash
        $  mvn clean package -DskipTests 
        ```
    * Ao final do comando a saída deve ser semelhante a:
        ```bash
        [INFO] ------------------------------------------------------------------------
        [INFO] BUILD SUCCESS
        [INFO] ------------------------------------------------------------------------
        [INFO] Total time: 1.215 s
        [INFO] Finished at: 2021-04-21T18:30:46-04:00
        [INFO] Final Memory: 28M/136M
        [INFO] ------------------------------------------------------------------------

        ```

    * `Executar` a aplicação com o perfil `dev`:
        ```bash
        $ java -jar  -Dspring.profiles.active=dev -Xmx1g -Xms256m ./target/cliente-1.0.0-SNAPSHOT.jar %
        ```
        * No final deve ter uma saída inicialização com sucesso semelhante a:
            ```bash
            ...

            2021-04-21 18:40:59.145  INFO 238963 --- [           main] o.s.b.a.e.web.EndpointLinksResolver      : Exposing 2 endpoint(s) beneath base path '/management'
            2021-04-21 18:40:59.486  INFO 238963 --- [           main] o.s.s.concurrent.ThreadPoolTaskExecutor  : Initializing ExecutorService 'applicationTaskExecutor'
            2021-04-21 18:41:00.281  INFO 238963 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
            2021-04-21 18:41:00.710  INFO 238963 --- [           main] b.c.a.CadastroDeClientesApplication      : Started CadastroDeClientesApplication in 6.359 seconds (JVM running for 6.848)

            ```    


## 2. Validar a implantação

1. Após a execução do processo de instalação, é necessário um tempo para a aplicação iniciar.

1. Para verificar, execute o comando abaixo:
    ```bash
    $ watch  podman-compose -f ./docker-compose.yml ps 
    ```
    * Geralmente o resultado sera semelhante a:
        ```bash

        using podman version: podman version 3.1.0
        podman ps -a --filter label=io.podman.compose.project=ms-cliente
        CONTAINER ID  IMAGE                                                COMMAND    CREATED         STATUS             PORTS                                           NAMES
        b66206f375d7  docker.elastic.co/elasticsearch/elasticsearch:7.9.3  eswrapper  25 minutes ago  Up 25 minutes ago  0.0.0.0:5432->5432/tcp, 0.0.0.0:9200->9200/tcp  es01
        5da63ae95466  docker.elastic.co/elasticsearch/elasticsearch:7.9.3  eswrapper  25 minutes ago  Up 25 minutes ago  0.0.0.0:5432->5432/tcp, 0.0.0.0:9200->9200/tcp  es02
        6a180f96cf7d  docker.elastic.co/elasticsearch/elasticsearch:7.9.3  eswrapper  25 minutes ago  Up 25 minutes ago  0.0.0.0:5432->5432/tcp, 0.0.0.0:9200->9200/tcp  es03
        0f92f517c163  docker.io/library/postgres:latest                    postgres   25 minutes ago  Up 25 minutes ago  0.0.0.0:5432->5432/tcp, 0.0.0.0:9200->9200/tcp  ms-cliente_postgres-compose_1
        0

        ```
1. Validar a saude da aplicação:
    ```bash
    $ curl -X GET "localhost:8080/management/health"
    ```
    * A saída de sucesso deste comando será semelhante abaixo:
        ```bash
        {
        "status" : "UP"
        }
        ```    


## 3. Referência

Foi utilizado o [seguinte](https://start.spring.io/#!type=maven-project&language=java&platformVersion=2.4.5.RELEASE&packaging=jar&jvmVersion=11&groupId=com.br.advline&artifactId=cadastro-cliente&name=Cadastro%20de%20clientes&description=Micro-servi%C3%A7o%20de%20gerenciamento%20de%20clientes&packageName=com.br.advline.cadastro-cliente&dependencies=lombok,hateoas,web,data-rest,liquibase,h2,postgresql,data-elasticsearch,actuator,cloud-config-client,data-jpa) gerador do spring para criar a base desta aplicação.

## 4. Documentação funcional
A documentação pode ser encontrada em [local](http://localhost:8080/swagger-ui/#/cliente) ou hospedado no [OpenShift](http://mscliente.advancedline.com.br/) por tempo limitado. A pagina é semelhate a:

![Screenshot](assets/image1.png)

## 5. Disponibilização em nuvem publica
Para a disponibilização desse desafio em nuvem publica, a imagem docker foi hospedada no [Docker Hub](https://hub.docker.com/r/lmborges/ms-cliente) e publicada no OpenShift utlizando o serviço Developer Sandbox(free trial). OpenShift dashboard:

![Screenshot](assets/image2.png)
