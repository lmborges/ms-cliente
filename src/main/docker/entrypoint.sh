#!/bin/sh

echo "The application will start in 2 s..." && sleep 2
exec java ${JAVA_OPTS} -Djava.security.egd=file:/dev/./urandom -jar "/home/jhipster/app.jar" "$@"
