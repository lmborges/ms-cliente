package br.com.advline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author lmborges
 *
 */
@SpringBootApplication
public class CadastroDeClientesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CadastroDeClientesApplication.class, args);
	}
}