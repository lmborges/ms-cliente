/**
 * 
 */
package br.com.advline.config;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.annotations.Api;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author lmborges
 *
 */
@Configuration
@EnableSwagger2
public class SpringFoxConfiguration {
	public static final Contact DEFAULT_CONTACT = new Contact("Leonardo Borges", "http://advsolucoes.com",
			"lborges.arch@gmail.com");

	public static final ApiInfo DEFAULT_API_INFO = new ApiInfo("Platform Builders Challenge", "É apenas um MVP. Diferenciais foram:"
			+ "<li>CQRS (Command Query Responsibility Segregation).</li><li>Liquibase.</li><li>Hateoas.</li><li>DDD.</li>", "1.0.0",
			"urn:tos", DEFAULT_CONTACT, "Apache 2.0", "http://www.apache.org/licenses/LICENSE-2.0", Arrays.asList());

	private static final Set<String> DEFAULT_PRODUCES_AND_CONSUMES = new HashSet<String>(
			Arrays.asList("application/json", "application/xml"));

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(DEFAULT_API_INFO)
				.produces(DEFAULT_PRODUCES_AND_CONSUMES)
				.consumes(DEFAULT_PRODUCES_AND_CONSUMES)
				.useDefaultResponseMessages(false)
				.select()
				.apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
				.paths(PathSelectors.any())
				.build();
	}

}
