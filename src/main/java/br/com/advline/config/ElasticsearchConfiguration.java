/**
 * 
 */
package br.com.advline.config;

import java.io.IOException;
import java.time.LocalDate;

import org.elasticsearch.client.Client;
import org.springframework.context.annotation.Bean;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

/**
 * @author lmborges
 *
 */
public class ElasticsearchConfiguration {

	/**
	 * @param client Client
	 * @return ElasticsearchTemplate
//	 */
//	@Bean
//	public ElasticsearchTemplate elasticsearchTemplate(Client client) {
//		return new ElasticsearchTemplate(client);
//	}

	/**
	 * Customização do EntityMapper
	 */
//	public static class CustomEntityMapper implements EntityMapper {
//
//		private ObjectMapper objectMapper;
//
//		/**
//		 * Customização do Mapper de Entidade
//		 */
//		public CustomEntityMapper() {
//			this.objectMapper = new ObjectMapper();
//
//			JavaTimeModule module = new JavaTimeModule();
////			module.addSerializer(LocalDate.class, new CustomLocalDateSerializer());
////			module.addDeserializer(LocalDate.class, new CustomLocalDateDeserializer());
//			objectMapper.registerModule(module);
//
//			objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
//		}
//
//		/**
//		 * {@inheritDoc}
//		 */
//		@Override
//		public String mapToString(Object object) throws IOException {
//			return objectMapper.writeValueAsString(object);
//		}
//
//		/**
//		 * {@inheritDoc}
//		 */
//		@Override
//		public <T> T mapToObject(String source, Class<T> clazz) throws IOException {
//			return objectMapper.readValue(source, clazz);
//		}
//	}
}
