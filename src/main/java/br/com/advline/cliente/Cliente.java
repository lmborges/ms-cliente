package br.com.advline.cliente;

import java.time.LocalDate;
import java.time.Period;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.hateoas.RepresentationModel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lmborges
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "cliente")
public class Cliente extends RepresentationModel<Cliente> {

	@Id
	@SequenceGenerator(name = "CLIENTE_ID_GENERATOR", sequenceName = "CLIENTE_PK_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLIENTE_ID_GENERATOR")
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "nome")
	private String nome;
	
	@Column(name = "data_nascimento")
	private LocalDate dtNascimento;
	
	@Transient
	public int getIdade() {
		return Period.between(this.dtNascimento, LocalDate.now()).getYears();
	}
}
