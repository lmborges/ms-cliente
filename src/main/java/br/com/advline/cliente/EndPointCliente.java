package br.com.advline.cliente;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.advline.util.rest.AbstractEndPoint;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;

/**
 * @author lmborges
 *
 */
@Api(tags = { "cliente" }, value = "cliente")
@RestController("cliente")
@AllArgsConstructor
public class EndPointCliente extends AbstractEndPoint implements SwaggerApiDocsCliente {

	private Clientes repositorio;

	@Override
	@GetMapping(value = "/cliente/{id:[0-9]+}", produces = "application/json")
	public HttpEntity<Cliente> obter(@PathVariable(value = "id") Long idCliente) {
		Cliente cliente = repositorio.obter(idCliente);
		cliente.add(linkTo(methodOn(EndPointCliente.class).obter(idCliente)).withSelfRel());
		return ResponseEntity.ok(cliente);
	}

	@Override
	@PostMapping(value = "/clientes", produces = "application/json")
	public ResponseEntity<EntityModel<Page<ClienteCache>>> listar(@RequestBody FiltroCliente cliente) {
		Page<ClienteCache> result = this.repositorio.listar(cliente.especificar(), cliente.paginar());
		List<ClienteCache> clienteComHyperMedia = result.getContent().stream()
				.map(cli -> cli.add(linkTo(methodOn(EndPointCliente.class).obter(cli.getId())).withSelfRel()))
				.collect(Collectors.toList());
		Page<ClienteCache> paginaClienteHypermedia = new PageImpl<ClienteCache>(clienteComHyperMedia, cliente.paginar(),
				result.getTotalElements());
		EntityModel<Page<ClienteCache>> retornoComHyperMedia = EntityModel.of(paginaClienteHypermedia,
				linkTo(methodOn(EndPointCliente.class).listar(cliente)).withSelfRel());
		return ResponseEntity.ok(retornoComHyperMedia);
	}

	@Override
	@PostMapping(value = "/cliente", produces = "application/json")
	@Transactional
	public HttpEntity<Cliente> cadastrar(@RequestBody Cliente cliente) {
		Cliente result = this.repositorio.salvar(cliente);
		result.add(linkTo(methodOn(EndPointCliente.class).obter(result.getId())).withSelfRel());
		return new ResponseEntity<Cliente>(result, HttpStatus.CREATED);
	}

	@Override
	@PutMapping(value = "/cliente/{id:[0-9]+}", produces = "application/json")
	@Transactional
	public HttpEntity<Cliente> atualizar(@RequestBody Cliente cliente, @PathVariable(value = "id") Long idCliente) {
		cliente.setId(idCliente);
		Cliente result = this.repositorio.salvar(cliente);
		result.add(linkTo(methodOn(EndPointCliente.class).obter(idCliente)).withSelfRel());
		return ResponseEntity.ok(result);
	}

	@Override
	@DeleteMapping(value = "/cliente/{id:[0-9]+}", produces = "application/json")
	@Transactional
	public HttpEntity deletar(@PathVariable(value = "id") Long idCliente) {
		repositorio.deletar(idCliente);
		return new ResponseEntity(HttpStatus.OK);
	}

}
