/**
 * 
 */
package br.com.advline.cliente;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import br.com.advline.util.ElasticSearchUtil;
import io.swagger.annotations.ApiParam;
import lombok.Data;

/**
 * @author lmborges
 *
 */
@Data
public class FiltroCliente {
	
    @ApiParam(
            value = "Nome do cliente (case insensitive, mas considera acentos (Andréia != Andreia))"
    )
    private String nome;
    
    @ApiParam(
            value = "Página da filtragem (começando do 0)",
            required = true,
            defaultValue = "0"
    )
    private Integer pagina;

    @ApiParam(
            value = "Quantidade de itens por página (default = 10)",
            defaultValue = "10"
    )
    private Integer porPagina;
    
    public BoolQueryBuilder especificar() {
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        if (nome != null)  ElasticSearchUtil.filtrar(queryBuilder, this.nome, "nome", Boolean.TRUE);
    	return queryBuilder;
    }
    public PageRequest paginar() {
        return PageRequest.of(this.pagina, this.porPagina);
    }
	
}
