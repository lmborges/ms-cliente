/**
 * 
 */
package br.com.advline.cliente;

import org.springframework.data.domain.Page;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author lmborges
 *
 */
public interface SwaggerApiDocsCliente {
	
	@ApiResponses(
			value = {
					@ApiResponse(
							code = 200,
							message = "OK",
							response = Cliente.class
					)
			}
	)
	@ApiOperation(
			value = "Endpoint para buscar um cliente pelo ID.",
			nickname = "obter",
			notes = "<li>Consulta CORPORATIVO.MUNICIPIO</li>",
			response = Cliente.class,
			tags = { "cliente" }
	)
	HttpEntity<Cliente> obter(Long idCliente);
	
	@ApiResponses(
			value = {
					@ApiResponse(
							code = 200,
							message = "OK",
							response = Cliente.class,
							responseContainer = "List"
					)
			}
	)
	@ApiOperation(
			value = "Endpoint para listar os clientes",
			nickname = "listar",
			notes = "<li>listar clientes</li>",
			response = Cliente.class,
			tags = { "cliente" }
	)
	ResponseEntity<EntityModel<Page<ClienteCache>>> listar(FiltroCliente cliente);

	@ApiResponses(
			value = {
					@ApiResponse(
							code = 200,
							message = "OK",
							response = Cliente.class
					)
			}
	)
	@ApiOperation(
			value = "Endpoint para cadastrar um cliente.",
			nickname = "cadastrar",
			notes = "<li>cadastrar cliente</li>",
			response = Cliente.class,
			tags = { "cliente" }
	)
	HttpEntity<Cliente> cadastrar(Cliente cliente);

	@ApiResponses(
			value = {
					@ApiResponse(
							code = 200,
							message = "OK",
							response = Cliente.class
					)
			}
	)
	@ApiOperation(
			value = "Endpoint para atualizar um cliente.",
			nickname = "atualizar",
			notes = "<li>atualizar cliente</li>",
			response = Cliente.class,
			tags = { "cliente" }
	)
	HttpEntity<Cliente> atualizar(Cliente cliente, Long idCliente);

	@ApiResponses(
			value = {
					@ApiResponse(
							code = 200,
							message = "OK",
							response = Cliente.class
					)
			}
	)
	@ApiOperation(
			value = "Endpoint para deletar um cliente pelo ID.",
			nickname = "deletar",
			notes = "<li>deletar cliente</li>",
			response = Cliente.class,
			tags = { "cliente" }
	)
	HttpEntity<Cliente> deletar(Long idCliente);

}