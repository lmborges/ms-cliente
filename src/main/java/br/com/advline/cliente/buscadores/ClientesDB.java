/**
 * 
 */
package br.com.advline.cliente.buscadores;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.advline.cliente.Cliente;

/**
 * @author lmborges
 *
 */
public interface ClientesDB extends JpaRepository<Cliente, Long>{

	
}
