/**
 * 
 */
package br.com.advline.cliente.buscadores;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import br.com.advline.cliente.ClienteCache;

/**
 * @author lmborges
 *
 */
public interface ClientesES extends ElasticsearchRepository<ClienteCache, Long>{
}

