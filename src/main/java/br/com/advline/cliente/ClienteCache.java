/**
 * 
 */
package br.com.advline.cliente;

import java.time.LocalDate;
import java.time.Period;

import javax.persistence.Id;
import javax.persistence.Transient;

import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.hateoas.RepresentationModel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lmborges
 *
 */
@Document(indexName = "cliente")
@Data
@AllArgsConstructor
public class ClienteCache extends RepresentationModel<ClienteCache> {

	@Id
	private Long id;
	@Field(type = FieldType.Text, store = true, analyzer = "trim_case_insensitive", fielddata = true)
	private String nome;
	@Field(type = FieldType.Date, format = DateFormat.custom, pattern = "yyyy-MM-dd")
	private LocalDate dtNascimento;
	
	public ClienteCache(Cliente cliente){
		super();
		this.id = cliente.getId();
		this.nome = cliente.getNome();
		this.dtNascimento = cliente.getDtNascimento();
	}
	
	@Transient
	public int getIdade() {
		return Period.between(this.dtNascimento, LocalDate.now()).getYears();
	}
}