/**
 * 
 */
package br.com.advline.cliente;

import java.util.List;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.advline.cliente.buscadores.ClientesDB;
import br.com.advline.cliente.buscadores.ClientesES;
import br.com.advline.util.erro.NaoEncontradoException;
import lombok.AllArgsConstructor;

/**
 * @author lmborges
 *
 */
@AllArgsConstructor
@Repository
public class Clientes {

	private ClientesDB db;
	private ClientesES es;

	public Page<ClienteCache> listar(BoolQueryBuilder specification, PageRequest pageRequest) {
		Page<ClienteCache> search = es.search(specification, pageRequest);
		return search;
	}

	public Cliente obter(Long id) {
		return db.findById(id).orElseThrow(NaoEncontradoException::new);
	}

	@Transactional(propagation = Propagation.MANDATORY)
	public Cliente salvar(Cliente cliente) {
		Cliente salvo = db.saveAndFlush(cliente);
		es.save(new ClienteCache(salvo));
		return salvo;
	}

	@Transactional(propagation = Propagation.MANDATORY)
	public void deletar(Long id) {
		db.deleteById(id);
		db.flush();
		es.deleteById(id);
	}
	
}
