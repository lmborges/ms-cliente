/**
 * 
 */
package br.com.advline.util;

import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

/**
 * @author lmborges
 *
 */
public class ElasticSearchUtil {
	/**
	 * Auxilia na construção de querys no Elastic
	 *
	 * @param boolQueryBuilder BoolQueryBuilder
	 * @param atributo      dado a ser utilizado na filtragem
	 * @param atributoName            campo a ser filtrado
	 */
	public static void filtrar(BoolQueryBuilder boolQueryBuilder, Object atributo, String atributoName,
			boolean isString) {
		if (atributo != null) {
			String filtro = atributo.toString();
			// Se não for um número
			if (isString) {
				boolQueryBuilder.must(QueryBuilders.wildcardQuery(atributoName,
						StringUtils.stripAccents(wrapStar(filtro).toLowerCase().trim())));
			} else {
				// Se for um número inteiro
				if (filtro.matches("^-?[0-9]+$")) {
					boolQueryBuilder.must(QueryBuilders.termQuery(atributoName, Long.valueOf(filtro)));
				}
				// Se for um número float/double
				else if (filtro.matches("^-?[0-9]+\\.?[0-9]+$")) {
					boolQueryBuilder.must(QueryBuilders.termQuery(atributoName, Double.valueOf(filtro)));
				}
			}
		}
	}

	private static String wrapStar(String field) {
		return ("*" + field + "*");
	}
}
