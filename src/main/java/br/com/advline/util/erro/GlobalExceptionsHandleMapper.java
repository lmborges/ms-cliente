/**
 * 
 */
package br.com.advline.util.erro;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;

/**
 * @author lmborges
 *
 */
@ControllerAdvice
@Slf4j
public class GlobalExceptionsHandleMapper {

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(NaoEncontradoException.class)
	@ResponseBody
	public Erro handleNaoEncontrado(HttpServletRequest req, NaoEncontradoException ex) {
		return Erro.builder().status(HttpStatus.NOT_FOUND).title("Não encontrado.")
				.mensagem("Item não encontrado").codigo("0004").build();
	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(NotFoundException.class)
	@ResponseBody
	public Erro handleNotFound(HttpServletRequest req, NotFoundException ex) {
		return this.handleNaoEncontrado(req, new NaoEncontradoException());
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(ServletRequestBindingException.class)
	@ResponseBody
	public Erro handleServletRequestBindingException(HttpServletRequest req, ServletRequestBindingException ex) {
		Pattern patternMissingHeader = Pattern.compile("Missing request header '(.*?)'");
		String detalhes = ex.getMessage();
		Matcher matcher = patternMissingHeader.matcher(detalhes);
		if (matcher.find()) {
			detalhes = "O cabeçalho '" + matcher.group(1) + "' deve ser informado";
		}
		return Erro.builder().status(HttpStatus.BAD_REQUEST).title("Requisição inválida.").mensagem(detalhes)
				.codigo("0000").build();
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Exception.class)
	@ResponseBody
	public Erro handleException(HttpServletRequest req, Exception ex) {
		log.error("Erro interno", ex);
		return Erro.builder().status(HttpStatus.INTERNAL_SERVER_ERROR).title("Erro interno.").codigo("0009")
				.mensagem("Erro generico:").detail(ex.getMessage()).build();
	}
}
