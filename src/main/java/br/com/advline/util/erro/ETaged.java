/**
 * 
 */
package br.com.advline.util.erro;

import java.math.BigInteger;

/**
 * @author lmborges
 * Objetos que tem um número (hash) indicando o seu estado.
 */
public interface ETaged {
	
	/**
	 * Um número grande o suficiente para mesmo após soma do menor e do maior {@link Integer} sempre terá a mesma quantidade de dígitos.
	 */
    BigInteger BASE = new BigInteger(Integer.toString(Integer.MIN_VALUE)).abs().multiply(new BigInteger("2")).add(new BigInteger("10").pow(10));
	
	/**
	 * Uma {@link String}, provavelmente apenas numérica, que indica o estado do objeto.
	 * @return Não pode ser null
	 */
	default String etag() {
		return BASE.add(new BigInteger(Integer.toString(this.hashCode()))).toString();
	}

}