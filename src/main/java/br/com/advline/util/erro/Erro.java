/**
 * 
 */
package br.com.advline.util.erro;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

/**
 * @author lmborges
 *
 */
@Getter
@Builder
@ToString
@ApiModel(description = "Um erro do micro-serviço. Seguindo a especificação do RFC 7870 - https://tools.ietf.org/html/rfc7807")
public class Erro implements ETaged {
	
	@NonNull
	@JsonIgnore
	@Builder.Default
	private HttpStatus status = Erro.exception("status");

	@Getter(lazy = true)
	@ApiModelProperty(
		value = 
			"Classificação do tipo de erro. "
			+ "Observar que essa URL pode ser relativa. "
			+ "Seguindo o template '/refs/erros/{CODIGO}' https://tools.ietf.org/html/rfc7807#section-3.1"
	)
	private final String type = String.format("/refs/erros/%s", this.codigo);
	
	@Builder.Default
	@ApiModelProperty(value = "Título do erro reportado pela aplicação")
	private String title = Erro.exception("title");

	@ApiModelProperty(value = "Detalhe do erro reportado pela aplicação")
	private String detail;

	@ApiModelProperty(value = "Detalhe do erro reportado pela aplicação para renderizar no Front-end")
	private String mensagem;
	
	@NonNull
	@Builder.Default
	@JsonProperty(value = "CODIGO")
	@ApiModelProperty(value = "Código interno do erro reportado pela aplicação")
	private String codigo = Erro.exception("codigo");


	@ApiModelProperty(value = "Instância do erro reportado pela aplicação")
	private String instance;
	
	@JsonProperty(value = "Status")
	@ApiModelProperty(value = "Código do erro reportado")
	private int getStatusCode() {
		return this.status.value();
	}

	@JsonIgnore
	private Throwable causa;

	public Erro.Excecao comoExcecao() {
		return this.causa == null ? new Excecao(this) : new Excecao(this, this.causa);
	}

	public static final <T> T exception(String atributo) throws IllegalStateException {
		throw new IllegalStateException(String.format("O atributo '%s' nunca foi informado", atributo));
	}

	public static class Excecao extends RuntimeException {

		/***/
		private static final long serialVersionUID = 6680705065938939048L;

		@Getter
		private Erro erro;

		public Excecao(Erro erro) {
			super();
			this.erro = erro;
		}

		public Excecao(Erro erro, Throwable t) {
			super(t);
			this.erro = erro;
		}

	}
	
}
