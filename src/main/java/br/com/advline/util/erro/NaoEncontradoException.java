/**
 * 
 */
package br.com.advline.util.erro;


/**
 * @author lmborges
 *
 */
public class NaoEncontradoException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public NaoEncontradoException() {
		super("Item solicitado não encontrado");
	}

}