/**
 * 
 */
package br.com.advline.util.rest;

import br.com.advline.util.erro.Erro;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author lmborges
 *
 */
@ApiResponses(
        value = {
                @ApiResponse(
                        code = 201,
                        message = "Criado com sucesso.",
                        response = Erro.class
                ),
                @ApiResponse(
                        code = 400,
                        message = "A requisição não foi mapeada para um recurso conhecido.",
                        response = Erro.class
                ),
                @ApiResponse(
                        code = 401,
                        message = "A requisição não contém autorização necessária (usuário não autenticado)",
                        response = Erro.class
                ),
                @ApiResponse(
                        code = 403,
                        message = "A requisição não contém permissão necessária (usuário não tem permissão)",
                        response = Erro.class
                ),
                @ApiResponse(
                        code = 404,
                        message = "A requisição não foi mapeada para um recurso conhecido.",
                        response = Erro.class
                ),
                @ApiResponse(
                        code = 500,
                        message = "Falha genérica na aplicação.",
                        response = Erro.class
                ),
        }
)
public abstract class AbstractEndPoint {

}
